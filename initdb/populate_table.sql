CREATE OR REPLACE FUNCTION insert_rows()
RETURNS VOID AS $$
    DECLARE 
        str VARCHAR(255) default 'John Doe';
        i INT8 default 0;
        cnt INT4 default 0;
        flag BOOLEAN default true;
        temp INT4 default 0;
        
    BEGIN
    WHILE i <= 1000
        LOOP
        flag = true;
        RAISE NOTICE '%', i;
        IF i % 2 = 0 THEN
            RAISE NOTICE '%', i % 2;
            flag = false;
        END IF;
        IF cnt >= 11 OR cnt = 0 THEN
            cnt = 1;
        END IF;
        temp = 2000 + cnt;
        
        INSERT INTO students (name, grade, active, admission_year) values (concat(str, i+1), cnt, flag, temp);
        i = i + 1;
        cnt = cnt + 1;
    END LOOP;
    END;
$$
LANGUAGE plpgsql;

SELECT insert_rows();
