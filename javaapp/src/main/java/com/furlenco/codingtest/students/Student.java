package com.furlenco.codingtest.students;


import javax.persistence.*;

@Entity
@Table(name = "students")
public class Student {


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;
    String name;
    int grade;
    boolean active;
    short admissionYear;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public short getAdmissionYear() {
        return admissionYear;
    }

    public void setAdmissionYear(short admissionYear) {
        this.admissionYear = admissionYear;
    }
}
