package com.furlenco.codingtest.students;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

import static com.furlenco.codingtest.students.StudentsSpecifications.*;

@RestController
public class StudentsController {

    @Autowired
    private StudentsService studentsService;

    @RequestMapping("/students")
    public List<Student> getStudents(
            @RequestParam(value = "grades", required = false) String grades,
            @RequestParam(value = "active", required = false) String active,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "admissionYearAfter", required = false) String admissionYearAfter,
            @RequestParam(value = "admissionYearBefore", required = false) String admissionYearBefore,
            @PageableDefault(page = 0, size = 20) Pageable pageable
    ) {
        Specification<Student> spec = Specification
                .where(admissionYearAfter(admissionYearAfter))
                .and(admissionYearBefore(admissionYearBefore))
                .and(isActive(active))
                .and(isNamePresent(name))
                .and(grades(grades))
                ;

        return studentsService.getStudents(spec, pageable);
    }

    @RequestMapping("/students/{id}")
    public ResponseEntity<Student> getStudent(@PathVariable long id) throws NotFoundException {
        Optional<Student> student = studentsService.getStudent(id);
        if (!student.isPresent()) {
            throw new NotFoundException("Student Not Found");
        }
        return new ResponseEntity<Student>(student.get(), HttpStatus.FOUND);
    }

    @RequestMapping(method= RequestMethod.POST, value="/students/")
    public ResponseEntity<Student> addStudent(@RequestBody Student student) {
        return new ResponseEntity<Student>(studentsService.addStudent(student), HttpStatus.CREATED);
    }

    @RequestMapping(method= RequestMethod.PATCH, value="/students/{id}")
    public ResponseEntity<Student> updateStudent(@PathVariable long id, @RequestBody Student student) {
        Optional<Student> studentExisting = studentsService.getStudent(id);
        BeanUtils.copyProperties(student, studentExisting.get(), "id", "name", "active", "admissionYear");
        return new ResponseEntity<Student>(studentsService.updateStudent(studentExisting.get()), HttpStatus.OK);
    }

    @RequestMapping(method= RequestMethod.DELETE, value="/students/{id}")
    public ResponseEntity<Student> deleteStudent(@PathVariable long id) {
        Optional<Student> studentExisting = studentsService.getStudent(id);
        return new ResponseEntity<Student>(studentsService.deleteStudent(studentExisting.get()), HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    public class NotFoundException extends RuntimeException {
        public NotFoundException(String exception) {
            super(exception);
        }
    }
}
