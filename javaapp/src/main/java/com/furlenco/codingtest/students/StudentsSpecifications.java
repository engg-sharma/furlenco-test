package com.furlenco.codingtest.students;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class StudentsSpecifications {
    public static Specification<Student> admissionYearAfter(String year) {
        return new Specification<Student>() {
            @Override
            public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                if (year == null) {
                    return null;
                } else {
                    return criteriaBuilder.greaterThanOrEqualTo(root.get("admissionYear"), year);
                }
            }
        };
    }

    public static Specification<Student> admissionYearBefore(String year) {
        return new Specification<Student>() {
            @Override
            public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                if (year == null) {
                    return null;
                } else {
                    return criteriaBuilder.lessThanOrEqualTo(root.get("admissionYear"), year);
                }
            }
        };
    }

    public static Specification<Student> isActive (String isActive) {
        return new Specification<Student>() {
            @Override
            public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                if (isActive == null) {
                    return null;
                } else {
                    return criteriaBuilder.equal(root.get("active"), Boolean.parseBoolean(isActive));
                }
            }
        };
    }

    public static Specification<Student> isNamePresent (String name) {
        return new Specification<Student>() {
            @Override
            public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                if (name == null) {
                    return null;
                } else {
                    return criteriaBuilder.equal(root.get("name"), name);
                }
            }
        };
    }

    public static Specification<Student> grades (String grade) {
        return new Specification<Student>() {
            @Override
            public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

                if (grade == null) {
                    return null;
                } else {
                    final List<Predicate> predicates = new ArrayList<Predicate>();
                    String[] arr = grade.split(",");

                    for (int i = 0; i < arr.length; i++) {
                        predicates.add(criteriaBuilder.equal(root.get("grade"), arr[i]));
                    }

                    return criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()]));
                }
            }
        };
    }
}
