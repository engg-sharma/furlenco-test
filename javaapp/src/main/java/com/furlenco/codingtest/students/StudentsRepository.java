package com.furlenco.codingtest.students;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface StudentsRepository extends JpaRepository<Student, Long>, JpaSpecificationExecutor<Student> {
    public List<Student> findByName(String name);
}
