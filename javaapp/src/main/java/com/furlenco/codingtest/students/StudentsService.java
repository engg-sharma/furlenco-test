package com.furlenco.codingtest.students;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentsService {

    @Autowired
    private StudentsRepository studentsRepository;

    public List<Student> getStudents(Specification<Student> specification, Pageable pageable) {
        List<Student> students = new ArrayList<>();
        studentsRepository.findAll(specification, pageable).forEach(students::add);
        return students;
    }

    public Optional<Student> getStudent(long id) {
        return studentsRepository.findById(id);
    }

    public Student addStudent(Student student) {
        student.setActive(true);
        return studentsRepository.save(student);
    }

    public Student updateStudent(Student student) {
        return studentsRepository.save(student);
    }

    public Student deleteStudent(Student student) {
        student.setActive(false);
        return studentsRepository.save(student);
    }
}
